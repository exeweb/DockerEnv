#!/bin/bash

# 根目录
APP_RTE_DIR=~/app-rte
# 开始菜单
start_menu() {
	clear
	echo -e "\033[40;37m ====================================================\033[0m"
	echo -e "\033[40;37m 介绍：app-rte环境 一键脚本           \033[0m"
	echo -e "\033[40;37m 系统：Ubuntu                        \033[0m"
	echo -e "\033[40;37m 防火墙：推荐使用云服务商防火(不启用防火墙) \033[0m"
	echo -e "\033[40;37m ====================================================\033[0m"
	echo
	echo -e "\033[0;33m 1. 系统 初始化\033[0m"
	echo -e "\033[0;33m 2. 安装 Docker\033[0m"
	echo -e "\033[0;33m 3. 安装 TEMTOP运行环境\033[0m"
	echo -e "\033[0;33m 4. 安装 wireguard\033[0m"
	echo -e "\033[0;33m 5. 安装 v2ray client\033[0m"
	echo -e " 0. 退出脚本"
	echo

  if [ -z "$1" ]; then
     read -p "请输入数字:" -r num
  else
      num="$1"
  fi

	case "$num" in
	1)
		os_install
		os_install_ret
		;;
	2)
		docker_install
		;;
	3)
		temtop_install
		;;
  4)
		wireguard_install
  ;;5)
    v2ray_install
  ;;
	0)
		exit 1
		;;
	*)
		clear
		echo -e "请输入正确数字:跳转中..."
		sleep 1s
		start_menu
		;;
	esac
}

docker_install() {
  # shellcheck source=install/docker/install.sh
	source ${APP_RTE_DIR}/install/docker/install.sh
}

temtop_install() {
  # shellcheck source=install/init/install_temtop.sh
	source ${APP_RTE_DIR}/install/init/install_temtop.sh
}


wireguard_install() {
  # shellcheck source=install/wireguard/install.sh
	source ${APP_RTE_DIR}/install/wireguard/install.sh
}

v2ray_install() {
  # shellcheck source=install/wireguard/install.sh
	source ${APP_RTE_DIR}/install/v2ray/install.sh
}

os_install_common(){
  # shellcheck source=install/init/install_common.sh
  source ${APP_RTE_DIR}/install/init/install_common.sh
}


os_install() {
  # 修改默认编辑器 sudo update-alternatives --config edito
  if ! command -v zip >/dev/null 2>&1; then
    echo "os_install is not installed. Installing..."
  else
    echo "os_install is already installed."
    return
  fi

  read -p "是否处理vim中文乱码？(y/n) " -r answer
  if [ "$answer" == "y" ]; then
    echo ':set encoding=utf-8' >> /etc/vim/vimrc
    echo "完成处理vim中文乱码"
  fi

  #read -p "是否初始化root密码？(y/n) " -r answer
  #if [ "$answer" == "y" ]; then
  #  echo "初始化设置密码 sudo passwd root"
  #  sudo passwd root
  #fi

  echo "备份源"
  sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
  echo "切换源"
  sudo tee /etc/apt/sources.list <<-'EOF'
deb http://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ jammy main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ jammy-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ jammy-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ jammy-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ jammy-backports main restricted universe multiverse
EOF

  echo "更新软件包缓存"
  sudo apt-get -y update

  echo "更新已安装软件"
  sudo apt-get -y upgrade

  echo "常用工具安装"
  sudo apt install -y net-tools lrzsz unzip zip apache2-utils mysql-client redis-tools etcd-client

  echo "修改时区 Asia/Shanghai"
  sudo timedatectl set-timezone Asia/Shanghai
  timedatectl show --property=Timezone --value
  echo "时间信息"
  timedatectl status
  read -p "是否定时同步时间？(y/n) " -r answer
  if [ "$answer" == "y" ]; then
    sudo apt install ntpdate
    echo "添加时同步任务(root)"
    sudo sh -c 'echo "*/5 * * * * /usr/sbin/ntpdate -u time2.aliyun.com >> /tmp/ntpdate.log 2>&1 &" >> /var/spool/cron/crontabs/root'
  fi
  service crond reload

  # 修改ssh最后执行
  read -p "是否修改ssh端口？(y/n) " -r answer
  if [ "$answer" == "y" ]; then
    echo "修改ssh端口"
    VARS_FILE=/etc/ssh/sshd_config
    VRRP_RULES="# 修改ssh端口\nPort 1060"
    if sudo grep -Fxq "# 修改ssh端口" "$VARS_FILE"; then
      echo "配置已添加"
    else
      echo "添加配置"
      sudo sed -i "/#Port 22/a $VRRP_RULES" "$VARS_FILE"
      sudo service ssh restart
    fi
  fi

  echo "重启cron"
  sudo systemctl restart cron

  echo "临时关闭swap"
  sudo swapoff -a
  echo "永久关闭swap(重启生效)"
  sudo sed -ri 's/.*swap.*/#&/' /etc/fstab
  echo "当前内存："
  free -m
}

os_install_ret(){
  #APP_RTE_DIR=~/app-rte
  if [ ! -d "$APP_RTE_DIR" ]; then
    echo "拉取app-rte项目"
    sudo git clone -b master --single-branch https://exeweb@gitee.com/exeweb/app-rte.git "$APP_RTE_DIR"
    cd "$APP_RTE_DIR" || exit
    echo "git忽略文件夹权限"
    sudo git config --global core.filemode false
    sudo git config --global user.email "exeweb@163.com"
    sudo git config --global user.name "linux"

    echo "git秘钥复制"
    sudo cp ${APP_RTE_DIR}/install/authorized/known_hosts /root/.ssh/known_hosts
    sudo cp ${APP_RTE_DIR}/install/authorized/id_ed25519 /root/.ssh/id_ed25519
    sudo chmod -R 600 /root/.ssh/id_ed25519

    echo "切换SSH仓库"
    sudo git remote rm origin
    sudo git remote add origin git@gitee.com:exeweb/app-rte.git
    echo "指定远程分支 --set-upstream-to=origin/master master"
    sudo git branch --set-upstream-to=origin/master master
  fi

  if [ ! -d "${APP_RTE_DIR}/data" ]; then
    echo "创建 ${APP_RTE_DIR}/data"
    mkdir -p "${APP_RTE_DIR}/data"
  fi

  os_install_common

  echo "os_install done"
}



start_menu "$@"
