if not exist mnt mkdir mnt
if not exist project mkdir project
if not exist images mkdir images
if not exist mysql\logs mkdir mysql\logs
if not exist mysql\data mkdir mysql\data
if not exist redis\data mkdir redis\data
if not exist rabbitmq\mnesia mkdir rabbitmq\mnesia
if not exist redis mkdir redis
if not exist nginx\log mkdir nginx\log
if not exist nginx\supervisor\log mkdir nginx\supervisor\log
if not exist php70-fpm\supervisor\log mkdir php70-fpm\supervisor\log
if not exist php72-fpm\supervisor\log mkdir php72-fpm\supervisor\log
if not exist php74-fpm\supervisor\log mkdir php74-fpm\supervisor\log
if not exist redis\log mkdir redis\log
if not exist rabbitmq\log mkdir rabbitmq\log
if not exist elk\elasticsearch\master\data mkdir elk\elasticsearch\master\data
if not exist elk\elasticsearch\master\logs mkdir elk\elasticsearch\master\logs
if not exist elk\elasticsearch\slave1\data mkdir elk\elasticsearch\slave1\data
if not exist elk\elasticsearch\slave2\data mkdir elk\elasticsearch\slave2\data
echo "done"
pause