docker save -o ./images/dockerenv_nginx.tar dockerenv_nginx:latest
docker save -o ./images/dockerenv_php70.tar dockerenv_php70:latest
docker save -o ./images/dockerenv_php72.tar dockerenv_php72:latest
docker save -o ./images/dockerenv_php74.tar dockerenv_php74:latest
docker save -o ./images/mysql.5.7.tar mysql:5.7
docker save -o ./images/redis.3.2.12.tar redis:3.2.12
::docker save -o ./images/kibana.7.5.1.tar kibana:7.5.1

::docker save -o ./images/logstash.7.9.0.tar logstash:7.9.0
::docker save -o ./images/elasticsearch.7.9.0.tar elasticsearch:7.9.0
::docker save -o ./images/elasticsearch-head.5.tar mobz/elasticsearch-head:5
pause