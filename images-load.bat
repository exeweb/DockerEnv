docker load -i ./images/dockerenv_nginx.tar
docker load -i ./images/dockerenv_php70.tar
docker load -i ./images/dockerenv_php72.tar
docker load -i ./images/dockerenv_php74.tar
docker load -i ./images/mysql.5.7.tar
docker load -i ./images/redis.3.2.12.tar
docker load -i ./images/kibana.7.5.1.tar
::docker load -i ./images/logstash.7.9.0.tar
::docker load -i ./images/elasticsearch.7.9.0.tar
::docker load -i ./images/elasticsearch-head.5.tar
pause